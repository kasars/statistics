import QtQuick 2.3
import QtQuick.Window 2.3
import CoronaStat 1.0

Window {
    id: mainWindow
    visible: true
    width: 800
    height: 480
    title: qsTr("Covid-19 Stat")

    AbstractQmlModel {
        id: cases
    }

    Component.onCompleted: {
        requestCases();
    }

    Text {
        id: count
        anchors {
            right: parent.right
            top: parent.top
        }
        text: qsTr("Cases: %1").arg(listView.count)
    }

    FilterProxyModel {
        id: filteredModel
        srcModel: cases
        filterRoleStr: "healthCareDistrict"
        passFilters: []
    }

    property variant districts: []
    property variant countInDistr: []


    ListView {
        id: listView
        anchors.fill: parent
        model: filteredModel

        delegate: Text {
            text: Qt.formatDate(new Date(model.date), "MM-dd") + " in: " + model.healthCareDistrict
        }
    }

    ListView {
        id: districtsView
        anchors {
            top: parent.top
            left: parent.horizontalCenter
            bottom: parent.bottom
        }
        width: mainWindow.width * 0.33
        spacing: 3

        model: districts.length

        delegate: Rectangle {
            width: districtsView.width
            height: districtsView.height/15;
            color: "#eeeeee"

            Text {
                anchors.centerIn: parent
                text: qsTr("%1 (%2)").arg(districts[modelData]).arg(countInDistr[modelData])
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (districts[modelData] === "Finland") {
                        filteredModel.passFilters = [];
                    }
                    else {
                        filteredModel.passFilters = [districts[modelData]];
                    }
                }
            }
        }
    }

    function handleJSON(json) {
        var c = json.confirmed;
        cases.clear();
        var distr = [];
        var count = [];
        for (var i=0; i<c.length; ++i) {
            cases.append(c[i]);
            var ind = distr.indexOf(c[i].healthCareDistrict);
            if (ind === -1) {
                distr.push(c[i].healthCareDistrict);
                count.push(1);
            }
            else {
                count[ind]++;
            }
        }
        distr.push("Finland");
        count.push(c.length);
        districts = distr;
        countInDistr = count;
    }

    function requestCases() {
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    var json = JSON.parse(xhr.responseText.toString());
                    handleJSON(json);
                }
                else {
                    console.log("Failed to read data");
                }
            }
        }

        xhr.open('GET', "https://w3qa5ydb4l.execute-api.eu-west-1.amazonaws.com/prod/finnishCoronaData", true);
        xhr.send('');
    }
}
