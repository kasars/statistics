/* ============================================================
 *
 * Copyright (C) 2016 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef AbstractQmlModel_H
#define AbstractQmlModel_H

#include <QObject>
#include <QString>
#include <QAbstractListModel>
#include <QVector>
#include <QVariant>

class AbstractQmlModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    explicit AbstractQmlModel(QObject *parent = 0);
    ~AbstractQmlModel();

    Q_INVOKABLE void append(const QVariantMap &item);
    Q_INVOKABLE void insert(int row, const QVariantMap &item);
    Q_INVOKABLE void clear();
    Q_INVOKABLE void removeAt(int row, int count = 1);

    int count() const;

    // QAbstractItemModel overrides
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

Q_SIGNALS:
    void countChanged();
    void rolesChanged();

private:
    QHash<int, QByteArray> m_modelRoles;
    QVector<QVariantMap>   m_modelData;
};

#endif
