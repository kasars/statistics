/* ============================================================
 *
 * Copyright (C) 2016 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef SortProxyModel_h
#define SortProxyModel_h

#include <QSortFilterProxyModel>

class SortProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int sortRole READ sortRole WRITE setSortRole NOTIFY sortRoleChanged)
    Q_PROPERTY(bool sortAscending READ sortAscending WRITE setSortAscending NOTIFY sortOrderChanged)
    Q_PROPERTY(QObject* srcModel READ srcModel WRITE setSrcModel NOTIFY srcModelChanged)

public:
    int sortRole() const;
    void setSortRole(int role);

    bool sortAscending() const;
    void setSortAscending(bool ascending);

    QObject *srcModel() const;
    void setSrcModel(QObject *model);

    Q_INVOKABLE int mapToSrcRow(int row) const;

Q_SIGNALS:
    void sortRoleChanged();
    void srcModelChanged();
    void sortOrderChanged();
};

#endif
