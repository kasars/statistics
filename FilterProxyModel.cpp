/* ============================================================
 *
 * Copyright (C) 2016 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "FilterProxyModel.h"

#include <QAbstractItemModel>
#include <QDebug>
#include <QModelIndex>


int FilterProxyModel::filterRole() const { return QSortFilterProxyModel::filterRole(); }
void FilterProxyModel::setFilterRole(int role)
{
    if (QSortFilterProxyModel::filterRole() != role) {
        QSortFilterProxyModel::setFilterRole(role);
        setFilterKeyColumn(0);
        invalidate();
        emit filtersChanged();
    }
}

int FilterProxyModel::roleFromString(const QString &role)
{
    QAbstractItemModel *model = sourceModel();
    if (!model) {
        return -1;
    }

    QHash<int, QByteArray> roleNames = model->roleNames();
    auto i = roleNames.constBegin();
    while (i != roleNames.constEnd()) {
        if (role == i.value()) {
            return i.key();
        }
        ++i;
    }
    return -1;
}

QString FilterProxyModel::filterRoleStr() const
{
    return m_filterRoleStr;
}

void FilterProxyModel::setFilterRoleStr(const QString &role)
{
    if (m_filterRoleStr == role) {
        return;
    }
    m_filterRoleStr = role;
    setFilterRole(roleFromString(role));
}


QObject *FilterProxyModel::srcModel() const { return sourceModel(); }

void FilterProxyModel::setSrcModel(QObject *model)
{
    QAbstractItemModel *aModel = qobject_cast<QAbstractItemModel *>(model);
    if (sourceModel() != aModel) {
        connect(aModel, &QAbstractItemModel::modelReset, this, [this]() { setFilterRole(roleFromString(m_filterRoleStr)); });
        setSourceModel(aModel);
        setFilterRole(roleFromString(m_filterRoleStr));
        invalidate();
        emit srcModelChanged();
    }
}

const QVariantList FilterProxyModel::passFilters() const { return m_passFilters; }
void FilterProxyModel::setPassFilters(const QVariantList &list)
{
    if (m_passFilters != list) {
        if (!list.isEmpty()) {
            m_blockFilters.clear();
        }
        m_passFilters = list;
        invalidate();
        emit filtersChanged();
    }
}

const QVariantList FilterProxyModel::blockFilters() const { return m_blockFilters; }
void FilterProxyModel::setBlockFilters(const QVariantList &list)
{
    if (m_blockFilters != list) {
        if (!list.isEmpty()) {
            m_passFilters.clear();
        }
        m_blockFilters = list;
        invalidate();
        emit filtersChanged();
    }
}

int FilterProxyModel::mapToSrcRow(int row) const
{
    QModelIndex srcIndex = mapToSource(index(row, 0));
    return srcIndex.row();
}

bool FilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QAbstractItemModel *model = qobject_cast<QAbstractItemModel *>(sourceModel());
    if (!model) {
        return false;
    }

    QModelIndex index = model->index(sourceRow, 0, sourceParent);
    QVariant data = index.data(filterRole());

    if (m_passFilters.isEmpty() && m_blockFilters.isEmpty()) {
        return true;
    }
    if (!m_passFilters.isEmpty() && m_passFilters.contains(data)) {
        return true;
    }
    if (!m_blockFilters.isEmpty() && !m_blockFilters.contains(data)) {
        return true;
    }
    return false;
}
