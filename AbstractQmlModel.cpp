/* ============================================================
 *
 * Copyright (C) 2020 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "AbstractQmlModel.h"


#include <QDebug>



AbstractQmlModel::AbstractQmlModel(QObject *parent) : QAbstractListModel(parent)
{
}

AbstractQmlModel::~AbstractQmlModel()
{
}

void AbstractQmlModel::append(const QVariantMap &item)
{
    insert(m_modelData.count(), item);
}

void AbstractQmlModel::insert(int row, const QVariantMap &item)
{
    if (row < 0 || row > m_modelData.count()) {
        qWarning() << "Invalid row on insert";
        return;
    }

    bool doReset = false;
    for (const QString key: item.keys()) {
        if (!m_modelRoles.values().contains(key.toUtf8())) {
            int newRole = m_modelRoles.count() + Qt::UserRole;
            m_modelRoles.insert(newRole, key.toUtf8());
            doReset = true;
        }
    }
    if (doReset) {
        beginResetModel();
        endResetModel();
    }

    beginInsertRows(QModelIndex(), row, row);
    m_modelData.insert(row, item);
    endInsertRows();

    emit countChanged();
}

void AbstractQmlModel::clear()
{
    beginResetModel();
    m_modelData.clear();
    m_modelRoles.clear();
    endResetModel();
    emit countChanged();
}

void AbstractQmlModel::removeAt(int row, int count)
{
    if (count == 0) return;

    if (row < 0 || row >= m_modelData.count()) {
        return;
    }

    if (row + count > m_modelData.count()) {
        qDebug() << "Trying to remove too much";
        count = m_modelData.count() - row;
    }

    beginRemoveRows(QModelIndex(), row, row + count -1);
    while (count > 0) {
        m_modelData.removeAt(row);
        count--;
    }
    endRemoveRows();
    emit countChanged();
}

QHash<int, QByteArray> AbstractQmlModel::roleNames() const
{
    return m_modelRoles;
}

int AbstractQmlModel::count() const
{
    return m_modelData.count();
}

int AbstractQmlModel::rowCount(const QModelIndex &) const
{
    return m_modelData.count();
}

QVariant AbstractQmlModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    int row = index.row();

    if (row >= m_modelData.size() || row < 0) {
        return QVariant();
    }

    QString strRole = m_modelRoles.value(role);
    const QVariantMap &rowData = m_modelData[row];

    return rowData.value(strRole, QVariant());
}


