#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "AbstractQmlModel.h"
#include "SortProxyModel.h"
#include "FilterProxyModel.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<AbstractQmlModel>     ("CoronaStat", 1, 0, "AbstractQmlModel");
    qmlRegisterType<SortProxyModel>   ("CoronaStat", 1, 0, "SortProxyModel");
    qmlRegisterType<FilterProxyModel> ("CoronaStat", 1, 0, "FilterProxyModel");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
